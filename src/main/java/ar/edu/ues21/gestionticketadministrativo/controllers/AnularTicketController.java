package ar.edu.ues21.gestionticketadministrativo.controllers;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ar.edu.ues21.gestionticketadministrativo.models.Greeting;

@RestController
public class AnularTicketController {

    private static final String template = "Anulacion ticket id %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/ticket")
    public Greeting greeting(@RequestParam(value="id", defaultValue="SN") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}